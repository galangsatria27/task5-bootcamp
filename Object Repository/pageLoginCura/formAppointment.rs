<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>formAppointment</name>
   <tag></tag>
   <elementGuidId>9928e527-3a75-4dd3-a25a-06c38be84c3d</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//form[@action='https://katalon-demo-cura.herokuapp.com/appointment.php#summary']</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>form.form-horizontal</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>form</value>
      <webElementGuid>b4994f74-d5ab-472b-a957-7ba25cfd57c8</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>form-horizontal</value>
      <webElementGuid>7ad87286-f868-4be0-bbb1-c25d4f8d1a06</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>action</name>
      <type>Main</type>
      <value>https://katalon-demo-cura.herokuapp.com/appointment.php#summary</value>
      <webElementGuid>d3ce84e7-f043-4834-9639-bf9ce184cf86</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>method</name>
      <type>Main</type>
      <value>post</value>
      <webElementGuid>a0c00d13-56e5-4b9f-9f31-b4b722439ea0</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>
                
                    Facility
                    
                        
                            Tokyo CURA Healthcare Center
                            Hongkong CURA Healthcare Center
                            Seoul CURA Healthcare Center
                        
                    
                
                
                    
                        
                             Apply for hospital readmission
                        
                    
                
                
                    Healthcare Program
                    
                        
                             Medicare
                        
                        
                             Medicaid
                        
                        
                             None
                        
                    
                
                
                    Visit Date (Required)
                    
                        
                            
                            
                                
                            
                        
                    
                
                
                    Comment
                    
                        
                    
                
                
                    
                        Book Appointment
                    
                
            </value>
      <webElementGuid>db471129-ea65-42e4-8633-b9beee29c20f</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;appointment&quot;)/div[@class=&quot;container&quot;]/div[@class=&quot;row&quot;]/form[@class=&quot;form-horizontal&quot;]</value>
      <webElementGuid>d29f831e-9665-4c57-b763-d2c27012bf17</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <type>Main</type>
      <value>//form[@action='https://katalon-demo-cura.herokuapp.com/appointment.php#summary']</value>
      <webElementGuid>68f4d4a7-789e-470f-af19-8de48a65f5fb</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//section[@id='appointment']/div/div/form</value>
      <webElementGuid>577c35cb-4674-4a6b-a952-17577c560ba5</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Make Appointment'])[2]/following::form[1]</value>
      <webElementGuid>c5e0734a-a6ff-415f-a767-36b65951a2c4</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Make Appointment'])[1]/following::form[1]</value>
      <webElementGuid>3036e85a-81ff-4f96-89fe-c9b160573351</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//form</value>
      <webElementGuid>95eba85a-b86d-49cf-a691-b84012e65546</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//form[(text() = '
                
                    Facility
                    
                        
                            Tokyo CURA Healthcare Center
                            Hongkong CURA Healthcare Center
                            Seoul CURA Healthcare Center
                        
                    
                
                
                    
                        
                             Apply for hospital readmission
                        
                    
                
                
                    Healthcare Program
                    
                        
                             Medicare
                        
                        
                             Medicaid
                        
                        
                             None
                        
                    
                
                
                    Visit Date (Required)
                    
                        
                            
                            
                                
                            
                        
                    
                
                
                    Comment
                    
                        
                    
                
                
                    
                        Book Appointment
                    
                
            ' or . = '
                
                    Facility
                    
                        
                            Tokyo CURA Healthcare Center
                            Hongkong CURA Healthcare Center
                            Seoul CURA Healthcare Center
                        
                    
                
                
                    
                        
                             Apply for hospital readmission
                        
                    
                
                
                    Healthcare Program
                    
                        
                             Medicare
                        
                        
                             Medicaid
                        
                        
                             None
                        
                    
                
                
                    Visit Date (Required)
                    
                        
                            
                            
                                
                            
                        
                    
                
                
                    Comment
                    
                        
                    
                
                
                    
                        Book Appointment
                    
                
            ')]</value>
      <webElementGuid>bc20e0d1-01ab-4f0f-8c0e-9d4037752651</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
