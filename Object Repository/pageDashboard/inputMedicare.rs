<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>inputMedicare</name>
   <tag></tag>
   <elementGuidId>e4e2c60f-3e34-4677-a069-26299feef4ac</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//input[@id='radio_program_medicare']</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>#radio_program_medicaid</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>input</value>
      <webElementGuid>f2d0aaec-e43b-450e-a082-3bf089f622b9</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>type</name>
      <type>Main</type>
      <value>radio</value>
      <webElementGuid>7db7f0fc-8875-4778-89d9-82981cb97ffb</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>name</name>
      <type>Main</type>
      <value>programs</value>
      <webElementGuid>68e59036-a7be-4451-a851-73e091f1e752</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>id</name>
      <type>Main</type>
      <value>radio_program_medicaid</value>
      <webElementGuid>a3995463-d5d1-4314-bb2b-ea9cfdb8812f</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>value</name>
      <type>Main</type>
      <value>Medicaid</value>
      <webElementGuid>e284ec77-92ca-4d73-abc6-6438be23bb49</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>wfd-id</name>
      <type>Main</type>
      <value>id2</value>
      <webElementGuid>7502095a-aff1-4fc4-aa3a-640e6c460088</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;radio_program_medicaid&quot;)</value>
      <webElementGuid>a09d4333-d1df-443b-bc0f-5719426b0590</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <type>Main</type>
      <value>//input[@id='radio_program_medicaid']</value>
      <webElementGuid>598b105e-4975-4180-b5ba-de38fab39ed0</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//section[@id='appointment']/div/div/form/div[3]/div/label[2]/input</value>
      <webElementGuid>2ff33a9e-33cc-487e-bd63-368413843b32</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//label[2]/input</value>
      <webElementGuid>db9cda8e-1d91-46b1-95c3-90c1cb62c45e</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//input[@type = 'radio' and @name = 'programs' and @id = 'radio_program_medicaid']</value>
      <webElementGuid>4d425add-5916-4eed-ab1e-2271c4bd941f</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
